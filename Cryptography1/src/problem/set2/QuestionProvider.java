package problem.set2;

import java.util.ArrayList;
import java.util.List;

import problem.set2.Question.QuestionType;

public class QuestionProvider {

	private QuestionProvider() {	}

	public static List<Question> getProblemSet2Questions() {
		List<Question> questions = new ArrayList<>(4);
		
		final String cbcKey1 = "140b41b22a29beb4061bda66b6747e14";
		final String cbcCiphertext1 = "4ca00ff4c898d61e1edbf1800618fb2828a226d160dad07883d04e008a7897ee2e4b7465d5290d0c0e6c6822236e1daafb94ffe0c5da05d9476be028ad7c1d81";
		Question q1 = new Question(cbcKey1, cbcCiphertext1, QuestionType.CBC);
		questions.add(q1);
		
		final String cbcKey2 = "140b41b22a29beb4061bda66b6747e14";
		final String cbcCiphertext2 = "5b68629feb8606f9a6667670b75b38a5b4832d0f26e1ab7da33249de7d4afc48e713ac646ace36e872ad5fb8a512428a6e21364b0c374df45503473c5242a253";
		Question q2 = new Question(cbcKey2, cbcCiphertext2, QuestionType.CBC);
		questions.add(q2);
		
		final String ctrKey3 = "36f18357be4dbd77f050515c73fcf9f2";
		final String ctrCiphertext3 = "69dda8455c7dd4254bf353b773304eec0ec7702330098ce7f7520d1cbbb20fc388d1b0adb5054dbd7370849dbf0b88d393f252e764f1f5f7ad97ef79d59ce29f5f51eeca32eabedd9afa9329";
		Question q3 = new Question(ctrKey3, ctrCiphertext3, QuestionType.CTR);
		questions.add(q3);
		
		final String ctrKey4 = "36f18357be4dbd77f050515c73fcf9f2";
		final String ctrCiphertext4 = "770b80259ec33beb2561358a9f2dc617e46218c0a53cbeca695ae45faa8952aa0e311bde9d4e01726d3184c34451";
		Question q4 = new Question(ctrKey4, ctrCiphertext4, QuestionType.CTR);
		questions.add(q4);
		
		return questions;
	}
}
