package problem.set2;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import problem.set2.Question.QuestionType;

public final class SolveRunner {

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		List<Question> questions = QuestionProvider.getProblemSet2Questions();
		
		for (Question q : questions) {
			QuestionType qt = q.getType();
			Cipher cipher = qt.getCipher();
			byte[] keyArray = q.getKeyAsByteArray();
			SecretKeySpec keySpec = new SecretKeySpec(keyArray, "AES");
			byte[] ivBytes = q.getIv();
			AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			byte[] plaintext = cipher.doFinal(q.getCiphertextAsByteArray());
			
			System.out.println("Mode: " + qt);
			System.out.println("Ciphertext: " + q.getCiphertext());
			System.out.println("Plaintext: " + new String(plaintext, "UTF-8"));
			System.out.println();
		}

	}

}
