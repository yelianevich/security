package problem.set2;

import java.security.NoSuchAlgorithmException;

import javax.crypto.*;
import javax.xml.bind.DatatypeConverter;

public class Question {
	private QuestionType type;
	private String key;
	private String ciphertext;
	
	public Question(String cbcKey, String cbcCiphertext, QuestionType type) {
		this.key = cbcKey;
		this.ciphertext = cbcCiphertext;
		this.type = type;
	}
	
	public QuestionType getType() {
		return type;
	}

	public void setType(QuestionType type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}
	
	public byte[] getKeyAsByteArray() {
		return DatatypeConverter.parseHexBinary(key);
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCiphertext() {
		return ciphertext;
	}
	
	public byte[] getCiphertextAsByteArray() {
		return DatatypeConverter.parseHexBinary(ciphertext.substring(32));
	}
	
	public byte[] getIv() {
		return DatatypeConverter.parseHexBinary(ciphertext.substring(0, 32));
	}

	public void setCiphertext(String ciphertext) {
		this.ciphertext = ciphertext;
	}

	public static enum QuestionType {
		CBC {
			@Override
			public Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
				return Cipher.getInstance("AES/CBC/PKCS5Padding");
			}
		},
		
		CTR {
			@Override
			public Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
				return Cipher.getInstance("AES/CTR/NoPadding");
			}
		};
		
		public abstract Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException;
	}
}
