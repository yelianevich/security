package problem.set1;


public final class SolveRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StreamCipherBreaker breaker = new StreamCipherBreaker();
		Key key = new Key(Ciphertexts.TARGET_CIPHERTEXT);
		for (int i = 0; i < Ciphertexts.CIPHERTEXTS.length; ++i) {
			breaker.analyze(Ciphertexts.TARGET_CIPHERTEXT, Ciphertexts.CIPHERTEXTS[i], key);
		}
		
		for (int i = 0; i < Ciphertexts.CIPHERTEXTS.length; ++i) {
			for (int j = 0; j < Ciphertexts.CIPHERTEXTS.length; ++j) {
				if (i != j) {
					breaker.analyze(Ciphertexts.CIPHERTEXTS[j], Ciphertexts.CIPHERTEXTS[j], key);
				}
			}
		}
		System.out.println(key);
		
		System.out.println(key.decode(Ciphertexts.TARGET_CIPHERTEXT));
	}
}
