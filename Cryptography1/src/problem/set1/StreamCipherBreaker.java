package problem.set1;

public class StreamCipherBreaker {
	
	/**
	 * XOR hex codes and returns ASCII representation
	 * @param c1 - msg in hex
	 * @param c2 - msg in hex
	 * @return ASCII representation of c1 XOR c2 operation
	 */
	public void analyze(String c1, String c2, Key key) {
		int l1 = c1.length();
		int l2 = c2.length();
		for (int i = 0; (i < l1 - 1) && (i < l2 - 1); i += 2) {
			int cipher1 = Integer.parseInt(c1.substring(i, i + 2), 16);
			int cipher2 = Integer.parseInt(c2.substring(i, i + 2), 16);
			int m1XORm2 = cipher1 ^ cipher2;
			if ((m1XORm2 >= 'A' && m1XORm2 <= 'Z')) {
				// it means that m1, m2 == space, [a-z]
				int tempKey1 = cipher1 ^ ' ';
				int tempKey2 = cipher1 ^ (m1XORm2 + Ciphertexts.LOWER_UPPER_DELTA);
				key.insertKey(tempKey1, tempKey2, i/2);
				int tempKey3 = cipher2 ^ ' ';
				int tempKey4 = cipher2 ^ (m1XORm2 + Ciphertexts.LOWER_UPPER_DELTA);
				key.insertKey(tempKey3, tempKey4, i/2);
			} else if ((m1XORm2 >= 'a' && m1XORm2 <= 'z')) {
				// it means that m1, m2 == space, [A-Z]
				int tempKey1 = cipher1 ^ ' ';
				int tempKey2 = cipher1 ^ (m1XORm2 - Ciphertexts.LOWER_UPPER_DELTA);
				key.insertKey(tempKey1, tempKey2, i/2);
				int tempKey3 = cipher2 ^ ' ';
				int tempKey4 = cipher2 ^ (m1XORm2 - Ciphertexts.LOWER_UPPER_DELTA);
				key.insertKey(tempKey3, tempKey4, i/2);
			}
		}
	}
}
