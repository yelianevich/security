package problem.set1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class Key {
	private final List<Set<Integer>> keys;

	public Key(String target) {
		System.out.println("msg symbols = " + target.length());
		keys = new ArrayList<>(target.length() / 2);
		for (int i = 0; i < target.length() / 2; ++i) {
			keys.add(new LinkedHashSet<Integer>());
		}
	}

	public String decode(String ciphertext) {
		System.out.println("Keys number = " + keys.size());
		StringBuilder ascii = new StringBuilder();
		int length = ciphertext.length();
		for (int i = 0; i < length; i += 2) {
			// grab the hex in pairs
			String output = ciphertext.substring(i, (i + 2));
			// convert hex to decimal
			int decimal = Integer.parseInt(output, 16);

			Set<Integer> keysVars = keys.get(i / 2);
			for (Integer key : keysVars) {
				char symbol = (char) (decimal ^ key);
				ascii.append(symbol);
				ascii.append(", ");
			}
			ascii.append("\n");
		}

		return ascii.toString();
	}

	public void insertKey(int k0, int k1, int pos) {

		// out of bounds
		if (pos >= keys.size()) {
			return;
		}
		keys.get(pos).add(k0);
		keys.get(pos).add(k1);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (Set<Integer> keySet : keys) {
			builder.append(keySet + "\n");
		}
		return builder.toString();
	}
}
