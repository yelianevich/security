package by.bsuir.security.hmac;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public final class HmacKeyManager {
	
	public static final String SECRET_NAME = "secret.key";
	private static final String HMAC_MD5_ALG = "HmacMd5";

	private HmacKeyManager() {
	}

	public static SecretKey generateAndSaveSecretKey(String secretFilename)
			throws NoSuchAlgorithmException, IOException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(HMAC_MD5_ALG);
		SecretKey secretKey = keyGenerator.generateKey();
		SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), HMAC_MD5_ALG);
		saveKeyToFs(keySpec, secretFilename);
		return secretKey;
	}

	public static boolean keyExists(String secret) {
		return Files.exists(Paths.get(secret), LinkOption.NOFOLLOW_LINKS);
	}

	public static SecretKey loadFromFs(String secretName) throws IOException,
			InvalidKeySpecException, NoSuchAlgorithmException {
		Path secretPath = Paths.get(secretName);
		byte[] rawKey = Files.readAllBytes(secretPath);
		SecretKeySpec keySpec = new SecretKeySpec(rawKey, HMAC_MD5_ALG);
		return keySpec;
	}
	
	private static void saveKeyToFs(SecretKey key, String filename) throws IOException {
		byte[] encodedKey = key.getEncoded();
		Files.write(Paths.get(filename), encodedKey, StandardOpenOption.CREATE);
	}
}
