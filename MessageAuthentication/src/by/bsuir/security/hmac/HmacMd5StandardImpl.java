package by.bsuir.security.hmac;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;

public final class HmacMd5StandardImpl implements HmacMd5 {

	@Override
	public byte[] getHmacMd5(byte[] initialMsg, SecretKey secretKey) throws NoSuchAlgorithmException, InvalidKeyException {
		Mac hmac = Mac.getInstance("HmacMd5");
		hmac.init(secretKey);
		byte[] mac = hmac.doFinal(initialMsg);
		return mac;
	}

}
