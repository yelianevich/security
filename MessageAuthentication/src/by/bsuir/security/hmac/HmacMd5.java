package by.bsuir.security.hmac;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.SecretKey;

public interface HmacMd5 {
	
	byte[] getHmacMd5(byte[] initialMsg, SecretKey secretKey) throws NoSuchAlgorithmException, InvalidKeyException;
	
}
