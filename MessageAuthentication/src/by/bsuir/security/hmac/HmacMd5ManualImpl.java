package by.bsuir.security.hmac;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.SecretKey;

public final class HmacMd5ManualImpl implements HmacMd5 {
	private static final int BLOCK_SIZE = 64;
	private static final byte[] OPAD = new byte[BLOCK_SIZE];
	private static final byte[] IPAD = new byte[BLOCK_SIZE];

	static {
		for (int i = 0; i < 64; ++i) {
			OPAD[i] = 0x36;
			IPAD[i] = 0x5C;
		}
	}
	
	@Override
	public byte[] getHmacMd5(byte[] initialMsg, SecretKey secretKey) throws NoSuchAlgorithmException {
		byte[] rawKey = secretKey.getEncoded();
		
		byte[] xorKeyOpad = new byte[BLOCK_SIZE];
		for (int i = 0; i < BLOCK_SIZE; ++i) {
			xorKeyOpad[i] = (byte) (0xFF & (rawKey[i] ^ OPAD[i]));
		}
		
		byte[] xorKeyIpad = new byte[BLOCK_SIZE];
		for (int i = 0; i < BLOCK_SIZE; ++i) {
			xorKeyIpad[i] = (byte) (0xFF & (rawKey[i] ^ IPAD[i]));
		}
		MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		messageDigest.update(xorKeyIpad);
		messageDigest.update(initialMsg);
		byte[] hashKeyXorIpadMsg = messageDigest.digest();

		messageDigest.update(xorKeyOpad);
		messageDigest.update(hashKeyXorIpadMsg);
		byte[] hmac = messageDigest.digest();
		return hmac;
	}

}
