package by.bsuir.security;

import static by.bsuir.security.rsa.KeyManagement.generateKeyPair;
import static by.bsuir.security.rsa.KeyManagement.keyPairExists;
import static by.bsuir.security.rsa.KeyManagement.loadKeyPair;
import static by.bsuir.security.rsa.KeyManagement.saveKeyPair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;

import by.bsuir.security.exeption.SignException;
import by.bsuir.security.exeption.VerifyException;

public final class DigitalSignatureTask {

	public void sign(String fileToSign, String signName)
			throws SignException {
		try {
			KeyPair keyPair = null;
			if (keyPairExists(".")) {
				keyPair = loadKeyPair(".", "RSA");
			} else {
				keyPair = generateKeyPair("RSA");
				saveKeyPair(".", keyPair);
			}
			byte[] initialMsg = Files.readAllBytes(Paths.get(fileToSign));
			Signature signature = Signature.getInstance("SHA512withRSA");
			signature.initSign(keyPair.getPrivate());
			signature.update(initialMsg);
			byte[] signa = signature.sign();
			Files.write(Paths.get(signName), signa, StandardOpenOption.CREATE);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException
				| IOException | InvalidKeyException | SignatureException e) {
			throw new SignException("Message signing failed", e);
		}
	}

	public boolean verify(String fileToVerify, String signName)
			throws VerifyException {
		try {
			KeyPair keyPair = null;
			if (keyPairExists(".")) {
				keyPair = loadKeyPair(".", "RSA");
			} else {
				throw new VerifyException("No key to sign message.");
			}
			byte[] initialMsg = Files.readAllBytes(Paths.get(fileToVerify));
			byte[] signature = Files.readAllBytes(Paths.get(signName));
			Signature sign = Signature.getInstance("SHA512withRSA");
			sign.initVerify(keyPair.getPublic());
			sign.update(initialMsg);
			return sign.verify(signature); 
		} catch (InvalidKeySpecException | NoSuchAlgorithmException
				| IOException | InvalidKeyException | SignatureException e) {
			throw new VerifyException("Message verification failed", e);
		}
	}

}
