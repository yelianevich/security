package by.bsuir.security;

import static by.bsuir.security.hmac.HmacKeyManager.SECRET_NAME;
import static by.bsuir.security.hmac.HmacKeyManager.generateAndSaveSecretKey;
import static by.bsuir.security.hmac.HmacKeyManager.keyExists;
import static by.bsuir.security.hmac.HmacKeyManager.loadFromFs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKey;

import by.bsuir.security.exeption.SignException;
import by.bsuir.security.exeption.VerifyException;
import by.bsuir.security.hmac.HmacMd5;

public final class HmacTask {

	public void sign(String fileToSign, String signName, HmacMd5 hmacMd5) throws SignException {
		try {
			SecretKey secretKey = keyExists(SECRET_NAME) ? loadFromFs(SECRET_NAME)
														 : generateAndSaveSecretKey(SECRET_NAME);
			byte[] initialMsg = Files.readAllBytes(Paths.get(fileToSign));
			byte[] mac = hmacMd5.getHmacMd5(initialMsg, secretKey);
			Files.write(Paths.get(signName), mac, StandardOpenOption.CREATE);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException
				| IOException | InvalidKeyException e) {
			throw new SignException("Message signing failed", e);
		}
	}

	public boolean verify(String fileToVerify, String signName, HmacMd5 hmacMd5) throws VerifyException {
		try {
			SecretKey secretKey =  null;
			if (keyExists(SECRET_NAME)) {
				secretKey = loadFromFs(SECRET_NAME);
			} else {
				throw new VerifyException("No key to verify HMAC");
			}
			byte[] initialMsg = Files.readAllBytes(Paths.get(fileToVerify));
			byte[] macCalc = hmacMd5.getHmacMd5(initialMsg, secretKey);
			byte[] macReceived = Files.readAllBytes(Paths.get(signName));
			return Arrays.equals(macCalc, macReceived) ? true : false; 
		} catch (InvalidKeySpecException | NoSuchAlgorithmException
				| IOException | InvalidKeyException e) {
			throw new VerifyException("Message verification failed", e);
		}
	}

}
