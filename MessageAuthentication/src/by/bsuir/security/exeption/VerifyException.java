package by.bsuir.security.exeption;

public class VerifyException extends Exception {
	private static final long serialVersionUID = 8620876762264759050L;

	public VerifyException() {
	}

	public VerifyException(String message) {
		super(message);
	}

	public VerifyException(Throwable cause) {
		super(cause);
	}

	public VerifyException(String message, Throwable cause) {
		super(message, cause);
	}

	public VerifyException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
