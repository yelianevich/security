package by.bsuir.security.exeption;

public class SignException extends Exception {
	private static final long serialVersionUID = 1212231052320389399L;

	public SignException() {
	}

	public SignException(String message) {
		super(message);
	}

	public SignException(Throwable cause) {
		super(cause);
	}

	public SignException(String message, Throwable cause) {
		super(message, cause);
	}

	public SignException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
