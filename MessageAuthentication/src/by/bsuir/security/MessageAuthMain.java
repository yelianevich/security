package by.bsuir.security;

import java.util.Scanner;

import by.bsuir.security.exeption.SignException;
import by.bsuir.security.exeption.VerifyException;
import by.bsuir.security.hmac.HmacMd5;
import by.bsuir.security.hmac.HmacMd5ManualImpl;
import by.bsuir.security.hmac.HmacMd5StandardImpl;

public final class MessageAuthMain {

	public static void main(String[] args) {
		String testFilename = "Test.txt";
		String signatureMan = "SignatureMan.sign";
		String signatureStd = "SignatureStd.sign";
		String signatureRsa = "SignatureRsa.sign";
		
		HmacTask hmacTask = new HmacTask();
		HmacMd5 standardHmac = new HmacMd5StandardImpl();
		HmacMd5 manualHmac = new HmacMd5ManualImpl();
		DigitalSignatureTask signatureTask = new DigitalSignatureTask();
		
		try {
			System.out.print("HMAC Manual: signing...");
			hmacTask.sign(testFilename, signatureMan, manualHmac);
			System.out.println(" signed.");
			System.out.print("HMAC SunJCE: signing...");
			hmacTask.sign(testFilename, signatureStd, standardHmac);
			System.out.println(" signed.");
			System.out.print("SHA-512 with RSA: signing...");
			signatureTask.sign(testFilename, signatureRsa);
			System.out.println(" signed.");
		} catch (SignException e) {
			System.err.println("Signing failed due to errors.");
			e.printStackTrace();
		}
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("You can make some changes in the test file. Then press Enter.");
			String str = scanner.nextLine();
			System.out.println(str);
		}
		try {
			System.out.println(hmacTask.verify(testFilename, signatureMan, manualHmac) 
					? "Proprietary HMAC is VALID."
					: "Proprietary HMAC is NOT valid. Message was corrupted.");
			System.out.println(hmacTask.verify(testFilename, signatureStd, standardHmac) 
					? "SunJCE HMAC is VALID."
					: "SunJCE HMAC is NOT valid. Message was corrupted.");
			System.out.println(signatureTask.verify(testFilename, signatureRsa)
					? "SHA-512 with RSA signature is VALID."
					: "SHA-512 with RSA verification NOT passed. Message was corrupted.");
		} catch (VerifyException e) {
			System.err.println("Verification failed due to errors");
			e.printStackTrace();
		}
	}

}
