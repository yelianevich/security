function Lsb(arrayBuffer, byteArray) {
	this.arrayBuffer = arrayBuffer;
	this.byteArray = byteArray;
	this.msgBytes = 0;
	console.log("LSB created");
	this.ondecoded = function() { 
		var imageEncoded = lsb.encodedImage();
		var imgElem = document.getElementById("decoded");
		imgElem.src = imageEncoded;
	}
	this.bmpOffset = 34;
}

Lsb.prototype.encodeMsg = function(msg) {
	this.msgBytes = msg.length * 2;
	var containerCapacityByte = this.arrayBuffer.byteLength / 4;

	if (containerCapacityByte < this.msgBytes) {
		alert("Container capacity is not enough");
		return;
	}

	// encode every message char
	var arrayIndex = this.bmpOffset;
	for (var j = 0; j < msg.length; ++j) {
		var msgChar = msg.charCodeAt(j);
		for (var bits = 0; bits < 8; bits += 1) {
			// clear last 2 bits
			var imageByte = this.byteArray.getUint8(arrayIndex);
			console.log(imageByte);
			imageByte = imageByte & 0xFC; // 1111 1100
			// ~
			var lastTwoBits = msgChar & 3;
			var encodedByte = imageByte | lastTwoBits;

			this.byteArray.setUint8(arrayIndex, encodedByte);

			msgChar = msgChar >>> 2;
			arrayIndex += 1;
		}
	}
};

Lsb.prototype.decodeMsg = function() {
	var decodedMsg = "";
	if (this.msgBytes == 0) {
		alert("No message in container");
		return;
	}

	// decode every message byte
	var arrayIndex = this.bmpOffset;
	for (var j = 0; j < this.msgBytes; j += 2) {
		var msgChar = 0;
		for (var imageByteNum = 0; imageByteNum < 8; ++imageByteNum) {
			var imageByte = this.byteArray.getUint8(arrayIndex);
			var twoLsb = imageByte & 0x03; // 0000 0011
			twoLsb = twoLsb << (imageByteNum * 2);
			msgChar = msgChar | twoLsb;

			arrayIndex += 1;
		}
		var encodedAsChar = String.fromCharCode(msgChar);
		decodedMsg = decodedMsg.concat(encodedAsChar);
	}
	alert("Initial Message Is: " + decodedMsg);
	this.ondecoded();
}

// public method for encoding an Uint8Array to base64
Lsb.prototype.encodedImage = function() {
    var arrayBuffer = this.arrayBuffer;
    var base64 = '',
    encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
    bytes = new Uint8Array(arrayBuffer), byteLength = bytes.byteLength,
    byteRemainder = byteLength % 3, mainLength = byteLength - byteRemainder,
    a, b, c, d, chunk;

	for (var i = 0; i < mainLength; i = i + 3) {
	    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
	    a = (chunk & 16515072) >> 18; b = (chunk & 258048) >> 12;
	    c = (chunk & 4032) >> 6; d = chunk & 63;
	    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
	}

	if (byteRemainder == 1) {
	    chunk = bytes[mainLength];
	    a = (chunk & 252) >> 2;
	    b = (chunk & 3) << 4;
	    base64 += encodings[a] + encodings[b] + '==';
	} else if (byteRemainder == 2) {
	    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
	    a = (chunk & 16128) >> 8;
	    b = (chunk & 1008) >> 4;
	    c = (chunk & 15) << 2;
	    base64 += encodings[a] + encodings[b] + encodings[c] + '=';
	}
	return "data:image/bmp;base64," + base64;

}