package by.bsuir.security;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyPair;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import by.bsuir.security.aes.AesCipher;
import by.bsuir.security.aes.AesKeyGenerator;
import by.bsuir.security.exception.KeysNotExist;
import by.bsuir.security.rsa.KeyManagement;
import by.bsuir.security.rsa.RsaCipher;

public class CryptoAsymmetricTask {
	
	private static final int AES_KEY_SPACE_ENCRYPTED = 128;
	private String alhorithm;
	
	public CryptoAsymmetricTask(String algorithm) {
		this.alhorithm = algorithm;
	}
	
	public boolean cipher(String inFile, String outFile) {
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		AesCipher aesCipher = new AesCipher();
		RsaCipher rsaCipher = new RsaCipher();
		SecretKey aesKey = AesKeyGenerator.generateSecretKey();

		try {
			KeyPair keyPair = null;
			if (KeyManagement.keyPairExists(".")) {
            	keyPair = KeyManagement.loadKeyPair(".", alhorithm);
            } else {
            	keyPair = KeyManagement.generateKeyPair(alhorithm);
            	KeyManagement.saveKeyPair(".", keyPair);
            }
	        byte[] rawBytes = Files.readAllBytes(Paths.get(inFile));
	        byte[] encryptedBytes = aesCipher.cipher(rawBytes, aesKey);
	        byteBuffer.write(encryptedBytes);
	        
	        // append aes key
	        byte[] aesKeyEncoded = rsaCipher.cipher(aesKey.getEncoded(), keyPair.getPublic());
	        byteBuffer.write(aesKeyEncoded);
	        Files.write(Paths.get(outFile), byteBuffer.toByteArray(), StandardOpenOption.CREATE);
		} catch (Exception e) {
			System.err.println("RSA/AES ciphering failed.");
			return false;
		}
		return true;
	}

	public boolean decipher(String inFile, String outFile) {
		AesCipher aesCipher = new AesCipher();
		RsaCipher rsaCipher = new RsaCipher();
		try {
			KeyPair keyPair = null;
            if (KeyManagement.keyPairExists(".")) {
            	keyPair = KeyManagement.loadKeyPair(".", alhorithm);
            } else {
            	throw new KeysNotExist();
            }
            byte[] encrypted = Files.readAllBytes(Paths.get(inFile));
            ByteBuffer buffer = ByteBuffer.wrap(encrypted); 
            
            // secret key restoring
            byte[] encryptedSessionKey = new byte[AES_KEY_SPACE_ENCRYPTED];
            buffer.position(buffer.capacity() - AES_KEY_SPACE_ENCRYPTED);
            buffer.get(encryptedSessionKey);
            byte[] sessionKeyRaw = rsaCipher.decipher(encryptedSessionKey, keyPair.getPrivate());
            SecretKey sessionKey = new SecretKeySpec(sessionKeyRaw, "AES");
            
            byte[] encryptedBytes = new byte[buffer.capacity() - AES_KEY_SPACE_ENCRYPTED];
	        buffer.position(0);
	        buffer.get(encryptedBytes);
            
            byte[] deciphered = aesCipher.decipher(encryptedBytes, sessionKey);
	        Files.write(Paths.get(outFile), deciphered, StandardOpenOption.CREATE);
	    } catch (Exception ex) {
	    	System.err.println("RSA/AES deciphering failed.");
			return false;
	    }
		return true;
	}
}
