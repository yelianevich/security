package by.bsuir.security;

import by.bsuir.security.util.OperationType;

public final class SecurityMain {

	public static void main(String[] args) {
		if (args.length != 4 && args.length != 3) {
			System.out.println("Application usage: ('cipher'|'decipher') (inFilename) (outFilename) [password]");
			return;
		}
		
		String mode = args[0];
		String inFilename = args[1];
		String outFilename = args[2];

		if (args.length == 3) {
			CryptoAsymmetricTask asymmetricTask = new CryptoAsymmetricTask("RSA");
			switch (OperationType.valueOf(mode.toUpperCase())) {
			case DECIPHER:
				if (!asymmetricTask.decipher(inFilename, outFilename)) {
					System.out.println("Cannot decipher");
				};
				break;
			case CIPHER:
				if (!asymmetricTask.cipher(inFilename, outFilename)) {
					System.out.println("Cannot cipher");
				}
				break;
			default:
				break;
			}
		}
		
		if (args.length == 4) {
			CryptoStreamTask streamTask = new CryptoStreamTask();
			String password = args[3];
			switch (OperationType.valueOf(mode.toUpperCase())) {
			case DECIPHER:
				if (!streamTask.decipher(inFilename, outFilename, password)) {
					System.out.println("Cannot decipher");
				}
				break;
			case CIPHER:
				if (!streamTask.cipher(inFilename, outFilename, password)) {
					System.out.println("Cannot cipher");
				}
				break;
			default:
				break;
			}
		}
		
		
	}

}
