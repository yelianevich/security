package by.bsuir.security.util;

public enum OperationType {
	CIPHER("cipher"), 
	DECIPHER("decipher");
	
	private String name;
	
	private OperationType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
