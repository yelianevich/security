package by.bsuir.security;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.crypto.SecretKey;

import by.bsuir.security.aes.AesCipher;
import by.bsuir.security.aes.AesKeyGenerator;

public class CryptoStreamTask {
	
	public boolean cipher(String inFile, String outFile, String password) {
		try {
			byte[] plain = Files.readAllBytes(Paths.get(inFile));
			System.out.println(new String(plain, "UTF-8"));
            SecretKey key = AesKeyGenerator.getPBSecretKey(password);
            AesCipher aesCipher = new AesCipher();
            byte[] encryptedBytes = aesCipher.cipher(plain, key);
            Files.write(Paths.get(outFile), encryptedBytes, StandardOpenOption.CREATE);
		} catch (Exception e) {
			System.err.println("Cipher process failed.");
			return false;
		}
		return true;
	}

	public boolean decipher(String inFile, String outFile, String password) {
		try {
			byte[] plain = Files.readAllBytes(Paths.get(inFile));
            SecretKey key = AesKeyGenerator.getPBSecretKey(password);
            AesCipher aesCipher = new AesCipher();
            byte[] encryptedBytes = aesCipher.decipher(plain, key);
            Files.write(Paths.get(outFile), encryptedBytes, StandardOpenOption.CREATE);
		} catch (Exception e) {
			System.err.println("Decipher process failed.");
			return false;
		}
		return true;
	}
}
