package by.bsuir.security.exception;

public class KeysNotExist extends Exception {
	private static final long serialVersionUID = 1L;
	
	public KeysNotExist() {
		super("Public/private pair does not exist.");
	}
	
	public KeysNotExist(String msg) {
		super(msg);
	}
}
