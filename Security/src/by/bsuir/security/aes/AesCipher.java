package by.bsuir.security.aes;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public final class AesCipher implements StreamCipher {
	
	@Override
	public byte[] cipher(byte[] plain, SecretKey key) {
		return transformBytes(Cipher.ENCRYPT_MODE, plain, key);
	}

	@Override
	public byte[] decipher(byte[] encrypted, SecretKey key) {
		return transformBytes(Cipher.DECRYPT_MODE, encrypted, key);
	}
	
	private byte[] transformBytes(int encryptMode, byte[] plain, SecretKey key) {
		byte[] encryptedBytes = null;
		try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	        cipher.init(encryptMode, key);
	        encryptedBytes = cipher.doFinal(plain);
		} catch (Exception e) {
			System.err.println("AES algorithm cannot perform given operation");
			encryptedBytes = new byte[0];
		}
		return encryptedBytes;
	}

}
