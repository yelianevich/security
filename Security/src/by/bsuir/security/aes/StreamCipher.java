package by.bsuir.security.aes;

import javax.crypto.SecretKey;

public interface StreamCipher {
	
	byte[] cipher(byte[] plain, SecretKey key);
	
	byte[] decipher(byte[] encrypted, SecretKey key);
}
