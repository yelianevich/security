package by.bsuir.security.aes;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public final class AesKeyGenerator {
	
	private static final String AES = "AES";
	private static final String PBKDF2_WITH_HMAC_SHA1 = "PBKDF2WithHmacSHA1";
	private static final int AES_KEY_SPACE = 256;
	private static final int ITERATION_COUNT = 65536;
	private static final byte[] SALT = {1,2,3,4,5,6,7,8};
	
	private AesKeyGenerator() {
	}
	
	public static SecretKey getPBSecretKey(String password) {
		SecretKey secret = null;
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance(PBKDF2_WITH_HMAC_SHA1);
			KeySpec spec = new PBEKeySpec(password.toCharArray(), SALT, ITERATION_COUNT, AES_KEY_SPACE);
			SecretKey tmp = factory.generateSecret(spec);
			secret = new SecretKeySpec(tmp.getEncoded(), AES);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("Cannot generate password based secret key.");
		}
		return secret;
	}
	
	public static SecretKey generateSecretKey() {
		SecretKey aesKey = null;
		try {
			KeyGenerator keygen = KeyGenerator.getInstance(AES);
			keygen.init(AES_KEY_SPACE);
			aesKey = keygen.generateKey();
		} catch (NoSuchAlgorithmException e) {
			System.err.println("AES algorithm is missing. Cannot generate session key.");
		}
		return aesKey;
	}
}
