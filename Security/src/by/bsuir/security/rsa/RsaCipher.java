package by.bsuir.security.rsa;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public final class RsaCipher implements AsymmetricCipher {

	@Override
	public byte[] cipher(byte[] plain, PublicKey publicKey) {
		byte[] cipherText = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			System.out.println("\nProvider = " + cipher.getProvider().getInfo());
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			cipherText = cipher.doFinal(plain);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			System.out.println("RSA cipher process failed.");
			cipherText = new byte[0];
		}
		return cipherText;
	}

	@Override
	public byte[] decipher(byte[] encrypted, PrivateKey privateKey) {
		byte[] plainText;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			plainText = cipher.doFinal(encrypted);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			System.out.println("RSA decipher process failed.");
			plainText = new byte[0];
		}
		return plainText;
	}
}
