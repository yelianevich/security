package by.bsuir.security.rsa;

import java.security.PrivateKey;
import java.security.PublicKey;

public interface AsymmetricCipher {
	
	byte[] cipher(byte[] plain, PublicKey publicKey);
	
	byte[] decipher(byte[] encripted, PrivateKey privateKey);
}
