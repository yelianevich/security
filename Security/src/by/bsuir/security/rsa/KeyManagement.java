package by.bsuir.security.rsa;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public final class KeyManagement {

	private static final int RSA_KEY_SPACE = 1024;
	private static final String PRIVATE_KEY_NAME = "/private.key";
	private static final String PUBLIC_KEY_NAME = "/public.key";
	
	private KeyManagement() {
	}
	
	public static KeyPair generateKeyPair(String algorithm) throws NoSuchAlgorithmException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
		kpg.initialize(RSA_KEY_SPACE);
	    KeyPair keyPair = kpg.generateKeyPair();
	    return keyPair;
	}
	
	public static boolean keyPairExists(String path) {
		boolean exists = Files.exists(Paths.get(path + PRIVATE_KEY_NAME), LinkOption.NOFOLLOW_LINKS)
				&& Files.exists(Paths.get(path + PUBLIC_KEY_NAME), LinkOption.NOFOLLOW_LINKS);
		return exists; 
	}
	
	public static KeyPair loadKeyPair(String path, String algorithm)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		byte[] encodedPublicKey = Files.readAllBytes(Paths.get(path
				+ PUBLIC_KEY_NAME));
		byte[] encodedPrivateKey = Files.readAllBytes(Paths.get(path
				+ PRIVATE_KEY_NAME));

		// Generate KeyPair.
		KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
				encodedPublicKey);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
				encodedPrivateKey);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
		return new KeyPair(publicKey, privateKey);
	}

	public static void saveKeyPair(String path, KeyPair keyPair) throws IOException {
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();

		// Store Public Key.
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
				publicKey.getEncoded());
		Files.write(Paths.get(path + PUBLIC_KEY_NAME),
				x509EncodedKeySpec.getEncoded(), StandardOpenOption.CREATE_NEW);

		// Store Private Key.
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
				privateKey.getEncoded());
		Files.write(Paths.get(path + PRIVATE_KEY_NAME),
				pkcs8EncodedKeySpec.getEncoded(), StandardOpenOption.CREATE_NEW);
	}

}
