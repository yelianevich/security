package by.bsuir.security.rsa;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.junit.Test;

public class RsaCipherTest {

	@Test
	public void test() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
		KeyPair keyPair = null;
		if (KeyManagement.keyPairExists(".")) {
        	keyPair = KeyManagement.loadKeyPair(".", "RSA");
        } else {
        	keyPair = KeyManagement.generateKeyPair("RSA");
        }
		
		String testStr = "Hello, cryptography world!";
		byte[] plain = testStr.getBytes("UTF-8");
		AsymmetricCipher rsaCipher = new RsaCipher();
		byte[] encrypted = rsaCipher.cipher(plain, keyPair.getPublic());
		byte[] restored = rsaCipher.decipher(encrypted, keyPair.getPrivate());
		
		String strRestore = new String(restored, "UTF-8");
		assertEquals(testStr, strRestore);
	}

}
