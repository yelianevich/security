package by.bsuir.security.aes;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;

import javax.crypto.SecretKey;

import org.junit.Test;

public class AesCipherTest {

	@Test
	public void test() throws UnsupportedEncodingException {
		StreamCipher aesCipher = new AesCipher();
		SecretKey key = AesKeyGenerator.getPBSecretKey("password");
		String testStr = "Hello, cryptography world!";
		byte[] encrypted = aesCipher.cipher(testStr.getBytes("UTF-8"), key);
		byte[] plain = aesCipher.decipher(encrypted, key);
		String strRestore = new String(plain, "UTF-8");
		assertEquals(testStr, strRestore);
	}

}
